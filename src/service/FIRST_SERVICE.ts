import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { environment } from '../environments/environment';
import { ToastController } from 'ionic-angular';




@Injectable()
export class AuthService {

  url = environment.backendUrl;
  myItem;
  signupId;

  constructor(private http: Http, public Toaser: ToastController) {
    if(JSON.parse(localStorage.getItem('userDatas'))){
      this.myItem = JSON.parse(localStorage.getItem('userDatas'));
      console.log(this.myItem,"000000053050523050");
      
      this.signupId=this.myItem[0]._id
  console.log(this.signupId);
    }


  }
  
  LoginSend(value) {
    // var data={
    //   "phone":"856934895689",
    //   "password":"nidhun"
    // }
    var data={
      "phone":value.phone,
      "password":value.password
    }
      console.log(data,"data here to send");
      
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(`${this.url}/login_pinto`, data).map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  
    }
    createCaty(value){
      value.signupId=this.signupId; 
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post(`${this.url}/create_category`, value).map(res => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
   
    }
    getCategory(){
      var data={
        "userId":this.signupId
      }
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      return this.http.post(`${this.url}/get_category`, data).map(res => res.json())
        .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
      
    }
    Deletecategory(id){
      var data={
        "deleteId":id
      }
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      return this.http.post(`${this.url}/delete_category`, data).map(res => res.json())
        .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
      
    }

    Editcategory(data){
   data.userId=this.signupId;
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      return this.http.post(`${this.url}/edit_category`, data).map(res => res.json())
        .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
      
    }
    addStudent(value){
  value.userId=this.signupId
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      return this.http.post(`${this.url}/add_student`, value).map(res => res.json())
        .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
      
    }
    //toster
    Mytoster(message){
      const toast = this.Toaser.create({
        message: message,
        duration: 3000,
        position: 'bottom'
      });
      toast.present();
    }
    getAllStudent(category){
      var data={
        "userId":this.signupId,
        "category":category
      }
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      return this.http.post(`${this.url}/all_student`, data).map(res => res.json())
        .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
       
    }
    getTdayActivity(activity){
      var data={
        "userId":this.signupId,
        "activity":activity
     
      }
      console.log(data,"send activity");
      
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      return this.http.post(`${this.url}/today_activity`, data).map(res => res.json())
        .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
       
    } 

    DeleteStudent(id){
      var data={
        "deleteId":id
      }
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      return this.http.post(`${this.url}/delete_student`, data).map(res => res.json())
        .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
      
    }
    //
    
    
    paymetupdate(data){
      data.userId=this.signupId;
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      return this.http.post(`${this.url}/do_payment`, data).map(res => res.json())
        .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
      
    }
    updatePayment(data){

      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      return this.http.post(`${this.url}/get_updated_payment`, data).map(res => res.json())
        .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
      
    }
}
