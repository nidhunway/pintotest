import { MenuPage } from './../menu/menu';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RegisterPage } from '../register/register';
import { AuthService } from '../../service/FIRST_SERVICE'

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  providers:[AuthService]
})
export class LoginPage {
  nidhun;
  constructor(public __FIST:AuthService,public navCtrl: NavController, public navParams: NavParams) {
  }




  Login(value)
  {
    if(value){
    this.__FIST.LoginSend(value).subscribe(
    res => {
       if(res.status){
     localStorage.setItem("userDatas",JSON.stringify(res.data));
     this.__FIST.Mytoster(res.message)
     this.navCtrl.setRoot(MenuPage);
    }else{
      this.__FIST.Mytoster(res.message)
    }},err=>{
      this.__FIST.Mytoster("something went wrong")

    });
      
    }
    
  }

}
