import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PizzamenuPage } from './pizzamenu';

@NgModule({
  declarations: [
    PizzamenuPage,
  ],
  imports: [
    IonicPageModule.forChild(PizzamenuPage),
  ],
})
export class PizzamenuPageModule {}
