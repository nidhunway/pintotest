import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthService } from '../../service/FIRST_SERVICE'
import { SMS } from '@ionic-native/sms';
import { AndroidPermissions } from '@ionic-native/android-permissions';


@IonicPage()
@Component({
  selector: 'page-broadcastmessage',
  templateUrl: 'broadcastmessage.html',
  providers:[AuthService,SMS,AndroidPermissions]
})
export class BroadcastmessagePage {
  data={
    "category":""
  }
  myCategory;
  constructor(public androidPermissions: AndroidPermissions,private sms: SMS,public __FIST:AuthService,public navCtrl: NavController, public navParams: NavParams) {
    this.getCategory()
  }

  getCategory(){
    this.__FIST.getCategory().subscribe(
      res => {
      if(res.status){
        this.myCategory=res.data;
      }else{
      }},err=>{
        this.__FIST.Mytoster("somthing went worng")
      });
  }
  
  send(){
    this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.SEND_SMS).then(
      (success) => {
        this.sms.send("8075607530","The msg from direction its a time to pay the remaining amount")
        .then((res)=>{
          console.log(res,"res");
          
          this.__FIST.Mytoster("Message send succesfully")
  
        },(err)=>{
          console.log(err,"sms");
          
          this.__FIST.Mytoster("Message send failed")
  
        });

      },(err)=>{
        console.log(err,"permision");
        
      }
    );
   }
}
