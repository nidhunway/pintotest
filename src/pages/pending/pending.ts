import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthService } from '../../service/FIRST_SERVICE'
import { CheckoutPage } from '../checkout/checkout';

/**
 * Generated class for the PendingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pending',
  templateUrl: 'pending.html',
  providers:[AuthService]
})
export class PendingPage {
  data={
    "category":""
  }
  allCategory;
  myCategory;
  GETstudents;
  constructor(public __FIST:AuthService,public navCtrl: NavController, public navParams: NavParams) {
    this.getCategory();
    this.getStudenInfo();

  }
  ionViewDidEnter(){
    this.getCategory();
    this.getStudenInfo();  }
  getCategory(){
    this.__FIST.getCategory().subscribe(
      res => {
      if(res.status){
        this.myCategory=res.data;
        this.myCategory.push({"category":"All"})
        
      }else{
      }},err=>{
        this.__FIST.Mytoster("somthing went worng")
      });
  }
//student
getTopics(ev: any) {
  // this.getStudenInfo();
  if(!ev.target.value){
    this.getStudenInfo();

  }
  console.log(ev.target.value,"fsfsffd")
  let serVal = ev.target.value;
  if (serVal && serVal.trim() != '') {
    this.GETstudents = this.GETstudents.filter((topic) => {
      console.log(topic.name,"fsdkjfdsgjkf");
      
      return (topic.name.toLowerCase().indexOf(serVal.toLowerCase()) > -1);
      
    })
    console.log(this.GETstudents,"1234555566");

  }
}
getStudenInfo(){
  this.__FIST.getAllStudent(this.allCategory).subscribe(
    res => {
      
    if(res.status){
      this.GETstudents=res.data;
      console.log(this.GETstudents,"123456")
      
    }else{
      this.__FIST.Mytoster(res.message)

      this.GETstudents="";
    }},err=>{
      this.__FIST.Mytoster("somthing went worng")
    });
}
//
payment(item){
  console.log(item);

  this.navCtrl.push(CheckoutPage,{"data":item});
  
}
onSelectChange(value){
       
  this.allCategory=value.category;
  console.log(this.allCategory);

  this.getStudenInfo();
}
}
