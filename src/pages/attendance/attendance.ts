import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { AuthService } from '../../service/FIRST_SERVICE'
import { AllCustomerPage } from '../all-customer/all-customer';

/**
 * Generated class for the AttendancePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-attendance',
  templateUrl: 'attendance.html',
  providers:[AuthService]
})
export class AttendancePage {
  myItem;
  constructor(public __FIST:AuthService,private alertCtrl: AlertController,public navCtrl: NavController, public navParams: NavParams) {
    this.myItem = JSON.parse(localStorage.getItem('profile'));
    console.log(this.myItem,"my item here");
    

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AttendancePage');
  }
  Delete(){
    let alert = this.alertCtrl.create({
      title: 'Confirm delete',
      message: 'Do you want to delete this class?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.conformDelete()
            console.log('Buy clicked');
          }
        }
      ]
    });
    alert.present();
  
  
  }
  conformDelete(){
    var id=this.myItem._id;
    this.__FIST.DeleteStudent(id).subscribe(
      res => {
      if(res.status){

        this.__FIST.Mytoster(res.message)
        this.navCtrl.push(AllCustomerPage);

      }else{
      this.__FIST.Mytoster(res.message)
    }},err=>{
        this.__FIST.Mytoster("somthing went worng")
      }); 
  }
}
