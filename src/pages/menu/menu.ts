import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { PizzamenuPage } from '../pizzamenu/pizzamenu';
import { CartPage } from '../cart/cart';
import { AllCustomerPage } from '../all-customer/all-customer';
import { TodayactivityPage } from '../todayactivity/todayactivity';
import { SettingsPage } from '../settings/settings';
import { AttendancePage } from '../attendance/attendance';
import { BroadcastmessagePage } from '../broadcastmessage/broadcastmessage';
import { PENDING } from '@angular/forms/src/model';
import { PendingPage } from '../pending/pending';


/**
 * Generated class for the MenuPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {
  UserData;
  constructor(public navCtrl: NavController, public navParams: NavParams,public menuCtrl: MenuController) {
    this.menuCtrl.swipeEnable;
    this.menuCtrl.enable(true);

    if(localStorage.getItem("userDatas"))this.UserData= JSON.parse(localStorage.getItem('userDatas'));
console.log(this.UserData,"9656824426466");


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MenuPage');
  }

  goPizza()
  {
    this.navCtrl.push(PizzamenuPage);
  }
goSettings(){
  this.navCtrl.push(SettingsPage);
}
goAttendance()
{
  this.navCtrl.push(AttendancePage);
}
goBroadcast(){
  this.navCtrl.push(BroadcastmessagePage);
}
  AllLogs()
  {
    this.navCtrl.push(AllCustomerPage);
  }

  // goCart()
  // {
  //   this.navCtrl.push(CartPage);
  // }
  // goAllCustomer()
  // {
  //   this.navCtrl.push(AllCustomerPage);
  // }
  AllPending()
  {
    this.navCtrl.push(PendingPage);
  
  }

  goTodayActivity()
  {
    this.navCtrl.push(TodayactivityPage)
  
  }

  openMenu() {
    this.menuCtrl.open();
  }

}
