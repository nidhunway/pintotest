import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegisterPage } from './register';
import { LottieAnimationViewModule } from 'ng-lottie';

@NgModule({
  declarations: [
    RegisterPage,
  ],
  imports: [
    IonicPageModule.forChild(RegisterPage),
    LottieAnimationViewModule
  ],
})
export class RegisterPageModule {}
