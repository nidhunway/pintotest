import { LoginPage } from './../login/login';
import { MenuPage } from './../menu/menu';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  public lottieConfig: Object;
  trigger:boolean;
  anim:any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.lottieConfig = {
      path: 'assets/icon/checked_done_.json',
      autoplay: true,
      loop: false
  };
  this.trigger = false;
  
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  Register()
  
  {
    this.showAnimation();
  }

  showAnimation()
{
  this.trigger = true;
  setTimeout(() => {
    this.navCtrl.setRoot(LoginPage);
}, 2000)
}

handleAnimation(anim: any) {
  this.anim = anim;
}

}
