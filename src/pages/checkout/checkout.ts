import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { PopupPage } from '../popup/popup';
import { MenuPage } from '../menu/menu';
import { LoadingController } from 'ionic-angular';
import { AuthService } from '../../service/FIRST_SERVICE'


@IonicPage()
@Component({
  selector: 'page-checkout',
  templateUrl: 'checkout.html',
  providers:[AuthService]
})
export class CheckoutPage {
  userdata;
  showDetails:boolean=false;
  showDetailss:boolean=false;
  data={
    "mode":"",
    "amount":"",
    "remark":""
  }
  constructor(public __FIST:AuthService,public loadingCtrl: LoadingController,public navCtrl: NavController, public navParams: NavParams,public modalCtrl: ModalController) {
    this.userdata=this.navParams.get('data');
    console.log(this.userdata,"1");
    

  }
  presentLoading() {
    const loader = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 3000
    });
    loader.present();
  }

  showPopup() {
    let profileModal = this.modalCtrl.create(PopupPage);
    profileModal.present();

    setTimeout(() => {
      profileModal.dismiss();
      this.navCtrl.setRoot(MenuPage);
  }, 1500);
  }
 
  toggleDetails() {
    
        this.showDetails = !this.showDetails;
 
  }
getUpdate(){
 

  this.__FIST.updatePayment(this.userdata).subscribe(
    res => {
      
    if(res.status){
      this.userdata=res.data[0];
      console.log(res.data[0],"2");


    }else{
      console.log("NO DATA FOUND");

    }},err=>{
      this.__FIST.Mytoster("somthing went worng")
    }); 
}
  AddCustomer(value)
  { 
    value.mode="Pending"
    value._id=this.userdata._id
   console.log(value,"to send")
    this.__FIST.paymetupdate(value).subscribe(
      res => {
      console.log(res,"update add")
      if(res.status){
        this.getUpdate();
        this.__FIST.Mytoster("Updated")

      }},err=>{
        this.__FIST.Mytoster("somthing went worng")
      });  
  }
}
