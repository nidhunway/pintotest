import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AllCustomerPage } from './all-customer';

@NgModule({
  declarations: [
  
    AllCustomerPage,
  ],
  imports: [
    IonicPageModule.forChild(AllCustomerPage),
  ],
  
})
export class AllCustomerPageModule {}
