import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { AuthService } from '../../service/FIRST_SERVICE'
import { AttendancePage } from '../attendance/attendance';
import { AlertController } from 'ionic-angular';

/**
 * Generated class for the AllCustomerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-all-customer',
  templateUrl: 'all-customer.html',
  providers:[AuthService]
})
export class AllCustomerPage {
data={
  "category":""
}
myCategory=[];
allCategory;
GETstudents;
searchTerm;
topics: string[];

  constructor(private alertCtrl: AlertController,public __FIST:AuthService,private sqlite: SQLite,public navCtrl: NavController, public navParams: NavParams) {
  this.getCategory();
  this.getStudenInfo();
  }
//serach
ionViewDidEnter(){
  this.getCategory();
  this.getStudenInfo();  }
getTopics(ev: any) {
  // this.getStudenInfo();
  if(!ev.target.value){
    this.getStudenInfo();

  }
  console.log(ev.target.value,"fsfsffd")
  let serVal = ev.target.value;
  if (serVal && serVal.trim() != '') {
    this.GETstudents = this.GETstudents.filter((topic) => {
      console.log(topic.name,"fsdkjfdsgjkf");
      
      return (topic.name.toLowerCase().indexOf(serVal.toLowerCase()) > -1);
      
    })
    console.log(this.GETstudents,"1234555566");

  }
}
  getCategory(){
    this.__FIST.getCategory().subscribe(
      res => {
      if(res.status){
        this.myCategory=res.data;
        this.myCategory.push({"category":"All"})

        console.log(this.myCategory,"categorymsdfdsgh4234 ");
        
      }else{
      }},err=>{
        this.__FIST.Mytoster("somthing went worng")
      });
  }
  viewALL(){
    this.allCategory="";

    this.getStudenInfo();
  }
  onSelectChange(value){
       
    this.allCategory=value.category;
    this.getStudenInfo();
}
getStudenInfo(){
  this.__FIST.getAllStudent(this.allCategory).subscribe(
    res => {
      
    if(res.status){
      this.GETstudents=res.data;
      
    }else{
      this.__FIST.Mytoster(res.message)

      this.GETstudents="";
    }},err=>{
      this.__FIST.Mytoster("somthing went worng")
    });
}

goProfile(value){
  localStorage.setItem("profile",JSON.stringify(value));
  
  this.navCtrl.push(AttendancePage);

}

Delete(id){
  let alert = this.alertCtrl.create({
    title: 'Confirm delete',
    message: 'Do you want to delete this class?',
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Yes',
        handler: () => {
          this.conformDelete(id)
          console.log('Buy clicked');
        }
      }
    ]
  });
  alert.present();


}
conformDelete(id){
  this.__FIST.DeleteStudent(id).subscribe(
    res => {
    if(res.status){
      this.getStudenInfo();
      this.__FIST.Mytoster(res.message)
    }else{
    this.__FIST.Mytoster(res.message)
  }},err=>{
      this.__FIST.Mytoster("somthing went worng")
    }); 
}

}
