import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TodayactivityPage } from './todayactivity';

@NgModule({
  declarations: [
    TodayactivityPage,
  ],
  imports: [
    IonicPageModule.forChild(TodayactivityPage),
  ],
})
export class TodayactivityPageModule {}
