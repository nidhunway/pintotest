import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PizzapagePage } from './pizzapage';

@NgModule({
  declarations: [
    PizzapagePage,
  ],
  imports: [
    IonicPageModule.forChild(PizzapagePage),
  ],
})
export class PizzapagePageModule {}
