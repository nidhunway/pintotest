import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { AuthService } from '../../service/FIRST_SERVICE'
import { AlertController } from 'ionic-angular';



@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
  providers:[AuthService]
})
export class SettingsPage {
  category='';
  items = [];
  myCategory;
  editid;
  updateButton:boolean=false;
  constructor(private alertCtrl: AlertController,public __FIST:AuthService,private sqlite: SQLite,public navCtrl: NavController, public navParams: NavParams) {
  this.getCategory()
  }
  save(){
    var data={
      "category":this.category.replace(/\s+/g, '')

    }
    this.__FIST.createCaty(data).subscribe(
    res => {
    if(res.status){
    this.__FIST.Mytoster(res.message)
     this.category="";
      this.getCategory();
      }else{
        this.__FIST.Mytoster(res.message)
      }},err=>{
        this.__FIST.Mytoster("somthing went worng")
      });
  }
  getCategory(){
    this.__FIST.getCategory().subscribe(
      res => {
        console.log(res,"response");
        
      if(res.status){
        this.myCategory=res.data;  
      }else{
      this.myCategory=""
     }},err=>{
        this.__FIST.Mytoster("No connection")
      });
  }
  Delete(id){
      let alert = this.alertCtrl.create({
        title: 'Confirm delete',
        message: 'Do you want to delete this class?',
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'Yes',
            handler: () => {
              this.conformDelete(id)
              console.log('Buy clicked');
            }
          }
        ]
      });
      alert.present();
    
  
  }
  conformDelete(id){
  this.__FIST.Deletecategory(id).subscribe(
      res => {
      if(res.status){
        this.getCategory();
        this.__FIST.Mytoster(res.message)
      }else{
      this.myCategory=""
      this.__FIST.Mytoster(res.message)
    }},err=>{
        this.__FIST.Mytoster("somthing went worng")
      }); 
  }
  //
  Update(){
    var data={
      "category":this.category.replace(/\s+/g, ''),
      "id":this.editid
    }
   this.__FIST.Editcategory(data).subscribe(
      res => {
      if(res.status){
        this.updateButton=false;
      this.getCategory();
      this.__FIST.Mytoster(res.message)
      this.category="";
      }else{
      this.__FIST.Mytoster(res.message)
      }},err=>{
        this.__FIST.Mytoster("somthing went worng")
      });     
  }
  Edit(id,data){
    this.updateButton=true
    this.editid=id;
    this.category=data;
  }

}
