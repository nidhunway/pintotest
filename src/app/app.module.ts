import { PopupPageModule } from './../pages/popup/popup.module';
import { CheckoutPageModule } from './../pages/checkout/checkout.module';
import { PizzapagePageModule } from './../pages/pizzapage/pizzapage.module';
import { MenuPageModule } from './../pages/menu/menu.module';
import { RegisterPageModule } from './../pages/register/register.module';
import { LoginPageModule } from './../pages/login/login.module';

import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { PizzamenuPageModule } from '../pages/pizzamenu/pizzamenu.module';
import { CartPageModule } from '../pages/cart/cart.module';
import { SQLite } from '@ionic-native/sqlite';

import { AllCustomerPageModule } from '../pages/all-customer/all-customer.module';
import { TodayactivityPageModule } from '../pages/todayactivity/todayactivity.module';
import { SettingsPageModule } from '../pages/settings/settings.module';
import { BroadcastmessagePageModule } from '../pages/broadcastmessage/broadcastmessage.module';
import { AttendancePageModule } from '../pages/attendance/attendance.module';
import {HttpModule} from '@angular/http';
import { Toast } from '@ionic-native/toast';
import {PendingPageModule} from '../pages/pending/pending.module';

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    
    LoginPageModule,
    RegisterPageModule,
    MenuPageModule,
    PizzamenuPageModule,
    PizzapagePageModule,
    CartPageModule,
    CheckoutPageModule,
    PopupPageModule,
   AllCustomerPageModule,
   TodayactivityPageModule,
   SettingsPageModule,
   BroadcastmessagePageModule,
   AttendancePageModule,
   PendingPageModule,
   HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    SQLite,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Toast
  ]
})
export class AppModule {}
