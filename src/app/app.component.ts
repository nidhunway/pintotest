import { CheckoutPage } from './../pages/checkout/checkout';
import { CartPage } from './../pages/cart/cart';
import { PizzapagePage } from './../pages/pizzapage/pizzapage';
import { MenuPage } from './../pages/menu/menu';
import { RegisterPage } from './../pages/register/register';

import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Menu } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPage } from '../pages/login/login';
import { PizzamenuPage } from '../pages/pizzamenu/pizzamenu';
import { PopupPage } from '../pages/popup/popup';
import { AllCustomerPage} from '../pages/all-customer/all-customer';
import { SettingsPage } from '../pages/settings/settings';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = LoginPage;

  pages: Array<{title: string, component: any,icon:string}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();
    if(localStorage.getItem("userDatas"))this.rootPage= MenuPage;
   console.log(localStorage.getItem("userData"),"52354u943693465");
   
    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: MenuPage, icon:"ios-home-outline" },
      { title: 'Class', component: SettingsPage, icon:"ios-contacts-outline" },
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  logout()
  {
    localStorage.removeItem('userDatas');

    this.nav.setRoot(LoginPage);
  }
}
